// import * as React from 'react';
import React, {useEffect, useState} from 'react';
import {Alert, Button, Text, TextInput, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useNavigation} from '@react-navigation/native';
import {SplashScreen, DetailScreen, HomeScreen, SignInScreen} from '../pages';
import AsyncStorage from '@react-native-community/async-storage';

// const AuthContext = React.createContext();
import AuthContext from '../pages/AuthContext';
const Stack = createStackNavigator();

export default function Router({navigation}) {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      // console.log('ini action.status', action.status);
      // console.log('ini action.function', action.type);
      switch (action.type) {
        case 'RESTORE_STATUS':
          return {
            ...prevState,
            statusAuth: action.status,
            isLoading: false,
            ses: true,
          };

        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            statusAuth: action.status,
          };

        case 'SIGN_UP':
          return {
            ...prevState,
            isSignout: true,
            // userToken: null,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            // userToken: null,
            statusAuth: false,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      statusAuth: false,
    },
  );

  React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let statusAuth;

      try {
        await AsyncStorage.getItem('dataLogin').then((data) => {
          statusAuth = JSON.parse(data);
          // console.log('ini data yg di get di home screen', statusAuth.success);
          // ses(statusAuth.success);
          // console.log('harus nya user id si user yang login', data.userId);

          console.log('test', statusAuth);

          if (statusAuth == null) {
            dispatch({type: 'RESTORE_STATUS', status: false});
          } else if (statusAuth.success == true) {
            dispatch({type: 'RESTORE_STATUS', status: statusAuth.success});
          } else {
            dispatch({type: 'RESTORE_STATUS', status: false});
          }

          console.log('ini console log dr restore', statusAuth.success);
          // console.log('ini console log dr state', state.ses);
        });

        // sesAuth = await AsyncStorage.getItem('sesAuth');
        // console.log('ini console log token login dari API :', dataLogin);
        //
      } catch (e) {
        // Restoring token failed
      }
    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo(
    () => ({
      signIn: async (data) => {
        try {
          console.log('ini username', data.username);
          console.log('ini password', data.password);
          if (data.username.length == 0 || data.password.length == 0) {
            Alert.alert(
              'Wrong input! ',
              'Username or password field cannot be empty.',
              [{text: 'Okay'}],
            );
          } else {
            fetch('http://150.242.110.240:8280/loginAppBonusTpay', {
              method: 'POST',
              headers: {
                Accept: 'application/json, text/plain',
                'Content-Type': 'application/json;charset=UTF-8',
              },
              body: JSON.stringify({
                userID: data.username,
                password: data.password,
              }),
            })
              .then((response) => response.json())
              .then(async (data) => {
                await AsyncStorage.setItem('dataLogin', JSON.stringify(data));

                dispatch({type: 'SIGN_IN', status: data.success});
              })
              .catch((error) => console.log('Error detected: ' + error));
          }
        } catch (error) {
          console.log(error);
        }
      },
      signOut: async () => {
        const keys = await AsyncStorage.getAllKeys();
        await AsyncStorage.multiRemove(keys);
        dispatch({type: 'SIGN_OUT'});
      },
    }),
    [],
  );

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <Stack.Navigator>
          {state.isLoading ? (
            // We haven't finished checking for the token yet
            <>
              <Stack.Screen name="Splash" component={SplashScreen} />
            </>
          ) : // <Stack.Screen name="Splash" component={SplashScreen} />
          state.statusAuth != true ? (
            // No token found, user isn't signed in
            <>
              <Stack.Screen
                name="SignIn"
                component={SignInScreen}
                options={{
                  title: 'Sign in',
                  // When logging out, a pop animation feels intuitive
                  animationTypeForReplace: state.isSignout ? 'pop' : 'push',
                }}
              />
            </>
          ) : (
            // User is signed in

            <>
              <Stack.Screen name="Home" component={HomeScreen} />
              <Stack.Screen name="Detail" component={DetailScreen} />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
}
