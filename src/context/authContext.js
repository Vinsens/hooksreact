// import * as React from 'react';
import React, {useEffect, useState, Fragment} from 'react';
// import {Alert, Button, Text, TextInput, View} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import AsyncStorage from '@react-native-community/async-storage';

export default function authContext() {
  const authContext = React.useMemo(
    () => ({
      signIn: async (data) => {
        try {
          console.log('ini username', data.username);
          console.log('ini password', data.password);
          if (data.username.length == 0 || data.password.length == 0) {
            Alert.alert(
              'Wrong input! ',
              'Username or password field cannot be empty.',
              [{text: 'Okay'}],
            );
          } else {
            fetch('http://150.242.110.240:8280/loginAppBonusTpay', {
              method: 'POST',
              headers: {
                Accept: 'application/json, text/plain',
                'Content-Type': 'application/json;charset=UTF-8',
              },
              body: JSON.stringify({
                userID: data.username,
                password: data.password,
              }),
            })
              .then((response) => response.json())
              .then(async (data) => {
                await AsyncStorage.setItem('dataLogin', JSON.stringify(data));

                dispatch({type: 'SIGN_IN', status: data.success});
              })
              .catch((error) => console.log('Error detected: ' + error));
          }
        } catch (error) {
          console.log(error);
        }
      },
      signOut: async () => {
        const keys = await AsyncStorage.getAllKeys();
        await AsyncStorage.multiRemove(keys);
        dispatch({type: 'SIGN_OUT'});
      },
    }),
    [],
  );
  return
}
