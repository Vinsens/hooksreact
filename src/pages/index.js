import * as React from 'react';
import HomeScreen from './HomeScreen';
import SignInScreen from './SignInScreen';
import DetailScreen from './DetailScreen';
import SplashScreen from './SplashScreen';

export {SplashScreen, HomeScreen, SignInScreen, DetailScreen};
