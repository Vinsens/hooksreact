// import * as React from 'react';
import React, {useEffect, useState} from 'react';
import {
  Alert,
  Button,
  Text,
  TextInput,
  View,
  KeyboardAvoidingView,
  ScrollView,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

import {AuthContext} from '../../App';

const SignInScreen = () => {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  const {signIn} = React.useContext(AuthContext);

  return (
    <KeyboardAvoidingView>
      <SafeAreaView>
        <View style={styles.container}>
          {/* <Text>UHUY</Text> */}
          <TextInput
            placeholder="Username"
            value={username}
            onChangeText={setUsername}
          />
          <TextInput
            placeholder="Password"
            value={password}
            onChangeText={setPassword}
            secureTextEntry
          />
          <Button
            title="Sign in"
            onPress={() => signIn({username, password})}
          />

          {/* <ButtonCheck /> */}
        </View>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    // paddingTop: 100,

    // backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  TitleSection: {
    marginTop: 20,
  },
  Card: {
    minHeight: 220,
    minWidth: 200,
  },

  Header: {
    backgroundColor: '#08d4c4',
  },
});
