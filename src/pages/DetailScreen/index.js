// import * as React from 'react';
import React, {useEffect, useState} from 'react';
import {Alert, Button, Text, TextInput, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useNavigation} from '@react-navigation/native';

import AsyncStorage from '@react-native-community/async-storage';

import {AuthContext} from '../../App';

function ButtonSignOut() {
  const {signOut} = React.useContext(AuthContext);
  return <Button title="SIGN OUT" onPress={signOut} />;
}

const checkSession = async function () {
  try {
    let userData = null;
    await AsyncStorage.getItem('dataLogin').then((data) => {
      userData = JSON.parse(data);
      console.log('ini data yg di get di home screen', userData);
      // setUname(userData.userId);
      // console.log('harus nya user id si user yang login', data.userId);
    });

    // console.log(b);
  } catch (error) {
    console.error('Error clearing app data.');
  }
};

function ButtonCheck() {
  return <Button title="CHECK SESSION" onPress={checkSession} />;
}

// function ButtonClear() {
//   // const {signOut} = React.useContext(AuthContext);
//   return <Button title="CLEAR SESSION" onPress={clearAppData} />;
// }

// function ButtonDetail({navigation}) {
//   // const {signOut} = React.useContext(AuthContext);
//   return (
//     <Button
//       title="Button Detail"
//       onPress={() => navigation.navigate('DetailScreen')}
//     />
//   );
// }

function DetailScreen({route, navigation}) {
  const [uname, setUname] = useState([]);
  const [enabled, setEnabled] = useState([]);

  const [name2, setName2] = useState([]);
  const {name} = route.params;
  useEffect(() => {
    async function fetchData() {
      let userData = null;
      await AsyncStorage.getItem('dataLogin').then((data) => {
        userData = JSON.parse(data);

        if (userData != null) {
          // console.log('ini data yg di get di home screen', data);
          setUname(userData.data[0].userId);
          setEnabled(userData.data[0].enabled);
          setName2(JSON.stringify(name));
        } else {
          alert('data Error');
        }
      });
    }
    fetchData();
  }, []);
  return (
    <View>
      <Text>Ini Detail Screen</Text>
      <Text>{uname}</Text>
      {/* <Text>{name}</Text> */}
      <Text>this is name send from home: {JSON.stringify(name)}</Text>
      <Text>this is name send from home use const :{name2}</Text>

      <Text>NAH KAN MASUK KE HALAMAN DETAIL SCREEN </Text>
      <Text>{enabled}</Text>
      <ButtonSignOut />
      <ButtonCheck />
      {/* <ButtonClear /> */}
      {/* <Button title="Navigate" onPress={() => navigation.navigate('Home')} /> */}
    </View>
  );
}

export default DetailScreen;
