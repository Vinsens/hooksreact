// import * as React from 'react';
import React, {useEffect, useState, useContext} from 'react';
import {
  Alert,
  Button,
  Text,
  TextInput,
  View,
  KeyboardAvoidingView,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useNavigation} from '@react-navigation/native';

import AsyncStorage from '@react-native-community/async-storage';
// import Router from './router';
// import AuthContext from '../AuthContext';
// const AuthContext = React.createContext();
import {AuthContext} from '../../App';

function ButtonSignOut() {
  const {signOut} = React.useContext(AuthContext);
  return <Button title="SIGN OUT" onPress={signOut} />;
}

const checkSession = async function () {
  try {
    let userData = null;
    await AsyncStorage.getItem('dataLogin').then((data) => {
      userData = JSON.parse(data);
      console.log('ini data yg di get di home screen', userData);
      // setUname(userData.userId);
      // console.log('harus nya user id si user yang login', data.userId);
    });

    // console.log(b);
  } catch (error) {
    console.error('Error clearing app data.');
  }
};

function ButtonCheck() {
  return <Button title="CHECK SESSION" onPress={checkSession} />;
}

function ButtonClear() {
  // const {signOut} = React.useContext(AuthContext);
  return <Button title="CLEAR SESSION" onPress={clearAppData} />;
}

function ButtonDetail({navigation}) {
  // const {signOut} = React.useContext(AuthContext);
  return (
    <Button
      title="Button Detail"
      onPress={() => navigation.navigate('DetailScreen')}
    />
  );
}
const HomeScreen = ({navigation, route}) => {
  const [uname, setUname] = useState([]);
  const [enabled, setEnabled] = useState([]);
  useEffect(() => {
    async function fetchData() {
      let userData = null;
      await AsyncStorage.getItem('dataLogin').then((data) => {
        userData = JSON.parse(data);

        if (userData != null) {
          // console.log('ini data yg di get di home screen', data);
          setUname(userData.data[0].userId);
          setEnabled(userData.data[0].enabled);
        } else {
          alert('data Error');
        }
      });
    }
    fetchData();
  }, []);
  return (
    <KeyboardAvoidingView>
      <SafeAreaView>
        <View>
          <Text>Signed in!</Text>
          <Text>{uname}</Text>
          <Text>EH NI ASUW UDAH BERHASIL </Text>
          <Text>{enabled}</Text>
          <ButtonSignOut />
          <ButtonCheck />
          <Button
            title="Button Detail"
            onPress={() => navigation.navigate('Detail', {name: {enabled}})}
            // onPress={() => navigation.navigate('Profile')}
          />
          {/* <ButtonDetail /> */}
          {/* <Button title="Navigate" onPress={() => navigation.navigate('Home')} /> */}
        </View>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

export default HomeScreen;
